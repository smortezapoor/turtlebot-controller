sudo apt update &&
sudo apt upgrade &&

export ROS_DISTRO=kinetic &&
sudo apt install ros-$ROS_DISTRO-turtlebot* ros-$ROS_DISTRO-astra-* -y &&
sudo apt install git chrony -y &&

echo export TURTLEBOT_BASE=kobuki >> ~/.bashrc &&
echo export TURTLEBOT_3D_SENSOR=astra >> ~/.bashrc &&
echo export TURTLEBOT_STACK=hexagons >> ~/.bashrc  &&

sudo apt-get install ntpdate -y &&

sudo ntpdate ntp.ubuntu.com &&

sudo apt install ros-$ROS_DISTRO-turtlebot-gazebo &&
sudo apt install ros-$ROS_DISTRO-turtlebot-apps &&
sudo apt install ros-$ROS_DISTRO-turtlebot-bringup &&
sudo apt install ros-$ROS_DISTRO-arbotix &&
sudo apt install ros-$ROS_DISTRO-yocs-velocity-smoother &&
sudo apt install ros-$ROS_DISTRO-explore-lite &&
sudo apt install ros-$ROS_DISTRO-robot-localization &&

cd /dev &&
sudo chmod og+rwx gpio* &&
cd  &&

sudo chmod 666 /dev/ttyUSB* &&

sudo apt install ros-$ROS_DISTRO-rplidar-ros &&