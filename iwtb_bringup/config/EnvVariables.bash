export TURTLEBOT_GAZEBO_WORLD_FILE='$(find iwtb_bringup)/gazebo/test_room_1.world'
export TURTLEBOT_BASE=kobuki
export TURTLEBOT_BATTERY=/proc/acpi/battery/BAT0
export TURTLEBOT_STACKS=hexagons
export TURTLEBOT_3D_SENSOR=astra